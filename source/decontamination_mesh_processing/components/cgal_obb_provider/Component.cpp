/**
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    decontamination_mesh_processing::ArmarXObjects::cgal_obb_provider
 * @author     Byungchul An ( byungchul dot an at gmail dot com )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Component.h"

#include <ArmarXCore/libraries/DecoupledSingleComponent/Decoupled.h>

// Include headers you only need in function definitions in the .cpp.

#include <VirtualRobot/Visualization/TriMeshModel.h>


namespace decontamination_mesh_processing::components::cgal_obb_provider
{

    const std::string
    Component::defaultName = "cgal_obb_provider";


    armarx::PropertyDefinitionsPtr
    Component::createPropertyDefinitions()
    {
        armarx::PropertyDefinitionsPtr def = new armarx::ComponentPropertyDefinitions(getConfigIdentifier());

        // Publish to a topic (passing the TopicListenerPrx).
        // def->topic(myTopicListener);

        // Subscribe to a topic (passing the topic name).
        // def->topic<PlatformUnitListener>("MyTopic");

        // Use (and depend on) another component (passing the ComponentInterfacePrx).
        // def->component(myComponentProxy)


        // Add a required property. (The component won't start without a value being set.)
        // def->required(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");

        // Add an optionalproperty.
//        def->optional(properties.boxLayerName, "p.box.LayerName", "Name of the box layer in ArViz.");
//        def->optional(properties.numBoxes, "p.box.Number", "Number of boxes to draw in ArViz.");

        return def;
    }


    void
    Component::onInitComponent()
    {
        // Topics and properties defined above are automagically registered.

        // Keep debug observer data until calling `sendDebugObserverBatch()`.
        // (Requies the armarx::DebugObserverComponentPluginUser.)
        // setDebugObserverBatchModeEnabled(true);
    }


    void
    Component::onConnectComponent()
    {
        // Do things after connecting to topics and components.

        /* (Requies the armarx::DebugObserverComponentPluginUser.)
        // Use the debug observer to log data over time.
        // The data can be viewed in the ObserverView and the LivePlotter.
        // (Before starting any threads, we don't need to lock mutexes.)
        {
            setDebugObserverDatafield("numBoxes", properties.numBoxes);
            setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
            sendDebugObserverBatch();
        }
        */

        /* (Requires the armarx::ArVizComponentPluginUser.)
        // Draw boxes in ArViz.
        // (Before starting any threads, we don't need to lock mutexes.)
        drawBoxes(properties, arviz);
        */

        /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
        // Setup the remote GUI.
        {
            createRemoteGuiTab();
            RemoteGui_startRunningTask();
        }
        */
    }


    void
    Component::onDisconnectComponent()
    {
    }


    void
    Component::onExitComponent()
    {
    }

    /// get optimal bounding box (oriented bounding box) from CGAL
    /// needs CGAL 5.1 and later
    /// needs Boost 1.66 and later
    armarx::Vector3dSeq Component::getCGALOBBVertices(const VRTriMeshParam &param, const Ice::Current &c)
    {
        armarx::Vector3dSeq obbpoints;
        VirtualRobot::TriMeshModel tm = getTriMeshModel(param);
        VirtualRobot::TriMeshModelPtr tmPtr(new VirtualRobot::TriMeshModel);
        tmPtr->vertices = tm.vertices;
        tmPtr->faces = tm.faces;
        tmPtr->normals = tm.normals;

        std::vector<Eigen::Vector3d> points = deconMP.getCGALOBBSurfaceMesh(tmPtr);

        for (const auto& point : points)
        {
            obbpoints.push_back({point(0), point(1), point(2)});
        }

        return obbpoints;
    }


    std::string
    Component::getDefaultName() const
    {
        return Component::defaultName;
    }


    std::string
    Component::GetDefaultName()
    {
        return Component::defaultName;
    }

    VirtualRobot::TriMeshModel Component::getTriMeshModel(const VRTriMeshParam &param)
    {
        VirtualRobot::TriMeshModel tm;
        std::vector<Eigen::Vector3f> vertices;
        std::vector<Eigen::Vector3f> normals;
        std::vector<VirtualRobot::MathTools::TriangleFace> faces;

        for (auto n : param.normals)
        {
            Eigen::Vector3f tempNormal = {n.e0, n.e1, n.e2};
            normals.push_back(tempNormal);
        }
        for (auto v : param.vertices)
        {
            Eigen::Vector3f tempV = {v.e0, v.e1, v.e2};
            vertices.push_back(tempV);
        }
        tm.normals = normals;
        tm.vertices = vertices;

        for (std::size_t i = 0; i < param.ids.size(); ++i)
        {
            VirtualRobot::MathTools::TriangleFace temp;
            temp.set(param.ids.at(i).e0, param.ids.at(i).e1, param.ids.at(i).e2);
            temp.setNormal(param.idNormals.at(i).e0, param.idNormals.at(i).e1, param.idNormals.at(i).e2);
            tm.addFace(temp);
        }


        return tm;
    }


    /* (Requires the armarx::LightweightRemoteGuiComponentPluginUser.)
    void
    Component::createRemoteGuiTab()
    {
        using namespace armarx::RemoteGui::Client;

        // Setup the widgets.

        tab.boxLayerName.setValue(properties.boxLayerName);

        tab.numBoxes.setValue(properties.numBoxes);
        tab.numBoxes.setRange(0, 100);

        tab.drawBoxes.setLabel("Draw Boxes");

        // Setup the layout.

        GridLayout grid;
        int row = 0;
        {
            grid.add(Label("Box Layer"), {row, 0}).add(tab.boxLayerName, {row, 1});
            ++row;

            grid.add(Label("Num Boxes"), {row, 0}).add(tab.numBoxes, {row, 1});
            ++row;

            grid.add(tab.drawBoxes, {row, 0}, {2, 1});
            ++row;
        }

        VBoxLayout root = {grid, VSpacer()};
        RemoteGui_createTab(getName(), root, &tab);
    }


    void
    Component::RemoteGui_update()
    {
        if (tab.boxLayerName.hasValueChanged() || tab.numBoxes.hasValueChanged())
        {
            std::scoped_lock lock(propertiesMutex);
            properties.boxLayerName = tab.boxLayerName.getValue();
            properties.numBoxes = tab.numBoxes.getValue();

            {
                setDebugObserverDatafield("numBoxes", properties.numBoxes);
                setDebugObserverDatafield("boxLayerName", properties.boxLayerName);
                sendDebugObserverBatch();
            }
        }
        if (tab.drawBoxes.wasClicked())
        {
            // Lock shared variables in methods running in seperate threads
            // and pass them to functions. This way, the called functions do
            // not need to think about locking.
            std::scoped_lock lock(propertiesMutex, arvizMutex);
            drawBoxes(properties, arviz);
        }
    }
    */


    /* (Requires the armarx::ArVizComponentPluginUser.)
    void
    Component::drawBoxes(const Component::Properties& p, viz::Client& arviz)
    {
        // Draw something in ArViz (requires the armarx::ArVizComponentPluginUser.
        // See the ArVizExample in RobotAPI for more examples.

        viz::Layer layer = arviz.layer(p.boxLayerName);
        for (int i = 0; i < p.numBoxes; ++i)
        {
            layer.add(viz::Box("box_" + std::to_string(i))
                      .position(Eigen::Vector3f(i * 100, 0, 0))
                      .size(20).color(simox::Color::blue()));
        }
        arviz.commit(layer);
    }
    */


    ARMARX_REGISTER_COMPONENT_EXECUTABLE(Component, Component::GetDefaultName());

}  // namespace decontamination_mesh_processing::components::cgal_obb_provider
