/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * package    decontamination_mesh_processing::cgal_obb_provider
 * author     Byungchul An ( byungchul dot an at gmail dot com )
 * date       2022
 * copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *            GNU General Public License
 */


#pragma once

#include <ArmarXCore/interface/serialization/Eigen.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/core/BasicVectorTypes.ice>

module decontamination_mesh_processing {  module components {  module cgal_obb_provider 
{
    struct VRTriMeshParam
    {
        armarx::Vector3fSeq vertices;
        armarx::Vector3fSeq normals;
        armarx::Vector3iSeq ids;
        armarx::Vector3iSeq idNormals;
    };


    interface ComponentInterface
    {
	// Define your interface here.
        armarx::Vector3dSeq getCGALOBBVertices(VRTriMeshParam param);
    };

};};};
