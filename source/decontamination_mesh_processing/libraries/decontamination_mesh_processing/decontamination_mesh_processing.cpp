/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    decontamination_mesh_processing::ArmarXObjects::decontamination_mesh_processing
 * @author     Byungchul An ( byungchul dot an at gmail dot com )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "decontamination_mesh_processing.h"
#include <VirtualRobot/Visualization/TriMeshModel.h>

namespace armarx
{

std::vector<Eigen::Vector3d> decontamination_mesh_processing::getCGALOBBSurfaceMesh(const VirtualRobot::TriMeshModelPtr &tm)
{
    DeconCGALOBB::SurfaceMesh sm = convertToCGALSurfaceMesh(*tm);
    std::array<DeconCGALOBB::Point, 8> obb_points_CGAL;
    CGAL::oriented_bounding_box(sm, obb_points_CGAL,
                                CGAL::parameters::use_convex_hull(true));

//    SimoxCGAL::CGALSurfaceMeshPtr scgalSM = SimoxCGAL::CGALMeshConverter::ConvertToSurfaceMesh(tm);
//    std::array<SimoxCGAL::Point, 8> obb_points;
//    CGAL::oriented_bounding_box(*scgalSM->getMesh(), obb_points,
//                                CGAL::parameters::use_convex_hull(true));

//    SimoxCGAL::SurfaceMesh obb_mesh;
//    CGAL::make_hexahedron(obb_points[0], obb_points[1], obb_points[2], obb_points[3],
//                          obb_points[4], obb_points[5], obb_points[6], obb_points[7], obb_mesh);
//    //        std::ofstream("test_simox_obb.off") << test_obb_mesh;

    std::vector<Eigen::Vector3d> obb_points;
    for (int i = 0; i < 8; ++i)
    {
        obb_points.push_back({obb_points_CGAL[i].x(), obb_points_CGAL[i].y(), obb_points_CGAL[i].z()});
    }

    return obb_points;
}

DeconCGALOBB::SurfaceMesh decontamination_mesh_processing::convertToCGALSurfaceMesh(const VirtualRobot::TriMeshModel &tm)
{
    DeconCGALOBB::SurfaceMesh sm;
    std::vector<DeconCGALOBB::SurfaceMesh::vertex_index> vidx;
    vidx.reserve(tm.vertices.size());
    for (const auto& v : tm.vertices)
    {
        vidx.emplace_back(sm.add_vertex({v(0), v(1), v(2)}));
    }
    for (const auto& f : tm.faces)
    {
        sm.add_face(vidx.at(f.id1), vidx.at(f.id2), vidx.at(f.id3));
    }

    return sm;
}

}
