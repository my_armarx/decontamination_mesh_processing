/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    decontamination_mesh_processing::ArmarXObjects::decontamination_mesh_processing
 * @author     Byungchul An ( byungchul dot an at gmail dot com )
 * @date       2022
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <VirtualRobot/VirtualRobot.h>
//#include <SimoxCGAL.h>

/***** CGAL Mesh processing *****/
//#include <CGAL/Simple_cartesian.h>


/***** CGAL Optimal Boundingbox *****/
#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/optimal_bounding_box.h>
#include <CGAL/Polygon_mesh_processing/triangulate_faces.h>
#include <CGAL/Polygon_mesh_processing/measure.h>

namespace DeconCGALOBB
{
    using Point = CGAL::Simple_cartesian<double>::Point_3;
    typedef CGAL::Surface_mesh<Point>                                       SurfaceMesh;
    typedef boost::shared_ptr<SurfaceMesh>                                  SurfaceMeshPtr;
}



/***** CGAL Mesh processing with Sharp Features *****/
//#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Mesh_triangulation_3.h>
#include <CGAL/Mesh_complex_3_in_triangulation_3.h>
#include <CGAL/Mesh_criteria_3.h>
#include <CGAL/Polyhedral_mesh_domain_with_features_3.h>
#include <CGAL/make_mesh_3.h>
#include <CGAL/IO/output_to_vtu.h>
#include <CGAL/refine_mesh_3.h>

#include <vector>
#include <Eigen/Core>

namespace DeconCGAL
{

    using SC = CGAL::Simple_cartesian<double>;

    /***** CGAL Mesh processing with Sharp Features *****/
    // Domain
    using K = CGAL::Exact_predicates_inexact_constructions_kernel;
    using Polyhedron = CGAL::Mesh_polyhedron_3<K>::type;
    using Mesh_domain = CGAL::Polyhedral_mesh_domain_with_features_3<K>;

#ifdef CGAL_CONCURRENT_MESH_3
    typedef CGAL::Parallel_tag Concurrency_tag;
#else
    typedef CGAL::Sequential_tag Concurrency_tag;
#endif

    // Triangulation
    using Tr = CGAL::Mesh_triangulation_3<Mesh_domain, CGAL::Default, Concurrency_tag>::type;
    using C3t3 = CGAL::Mesh_complex_3_in_triangulation_3<Tr, Mesh_domain::Corner_index, Mesh_domain::Curve_index>;

    // Criteria
    using Mesh_criteria = CGAL::Mesh_criteria_3<Tr>;
}
/***** CGAL Mesh processing with Sharp Features *****/

namespace armarx
{
    /**
    * @defgroup Library-decontamination_mesh_processing decontamination_mesh_processing
    * @ingroup decontamination_mesh_processing
    * A description of the library decontamination_mesh_processing.
    *
    * @class decontamination_mesh_processing
    * @ingroup Library-decontamination_mesh_processing
    * @brief Brief description of class decontamination_mesh_processing.
    *
    * Detailed description of class decontamination_mesh_processing.
    */
    class decontamination_mesh_processing
    {
    public:

        std::vector<Eigen::Vector3d> getCGALOBBSurfaceMesh(const VirtualRobot::TriMeshModelPtr& tm);

    private:
        DeconCGALOBB::SurfaceMesh convertToCGALSurfaceMesh(const VirtualRobot::TriMeshModel& tm);

    };

}
